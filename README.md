# bamtools Singularity container
### Bionformatics package bamtools<br>
BamTools provides both a programmer's API and an end-user's toolkit for handling BAM files.<br>
bamtools Version: 2.5.1<br>
[https://github.com/pezmaster31/bamtools]

Singularity container based on the recipe: Singularity.bamtools_v2.5.1

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bamtools_v2.5.1.sif Singularity.bamtools_v2.5.1`

### Get image help
`singularity run-help ./bamtools_v2.5.1.sif`

#### Default runscript: STAR
#### Usage:
  `bamtools_v2.5.1.sif --help`<br>
    or:<br>
  `singularity exec bamtools_v2.5.1.sif bamtools --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bamtools_v2.5.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/bamtools/bamtools:latest`


